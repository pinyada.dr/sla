from flask import Flask, request,jsonify
import mysql.connector
import datetime
import calendar

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

# Database connect
database_connect = mysql.connector.connect(
      host = "localhost",
      port="8889",
      user="",
      passwd="",
      database="sla"
)
data_cursor = database_connect.cursor()
date_now = datetime.datetime.now()
year_now = date_now.year
month_now = date_now.month
@app.route('/test' ,methods=['GET'])
def test():
    try:
        print("test api")
        return("test api")
    
    except Exception as e:
        print("error >>>>>>",e)

# Network 
@app.route('/add_network' ,methods=['POST'])
def add_data_network():
    try:
        data_nw = request.get_json()

        platform_network = data_nw['platform_network']
        network_per = data_nw['network_per']
        
        for (platform,percent) in zip(platform_network, network_per):
            sql_insert = "INSERT INTO network ( platform_network, network_per, create_date, update_date ) VALUES (%s,%s,%s,%s)"
            data_cursor.execute(sql_insert,(platform,percent,date_now,date_now))
            database_connect.commit()
        return ("Insert success")
        
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/network_month' ,methods=['GET'])
def get_network_month():
    try:
        sql_select = "SELECT FORMAT(Avg(network_per),2), month(update_date) FROM network WHERE YEAR(update_date) = %s GROUP BY month(update_date)"
        data_cursor.execute(sql_select,(year_now,))
        result = data_cursor.fetchall()

        avg_arr = []
        for avg in result:
            status = avg[0]
            month = calendar.month_name[avg[1]]
            norm_month = avg[1]
            data_set = {"norm_month":norm_month,"month":month,"status":status}
            avg_arr.append(data_set)
           
        return jsonify(avg_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/network_detail' ,methods=['POST'])
def get_network_detail():
    try:
        get_nw_month = request.get_json()
        month = get_nw_month['month']

        sql_select = "SELECT platform_network, FORMAT(Avg(network_per),2) FROM network WHERE MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY platform_network"
        data_cursor.execute(sql_select,(month,year_now))
        result = data_cursor.fetchall()

        detail_arr = []
        for detail in result:
            network = detail[0]
            status = detail[1]
            network_detail = {"network":network,"status":status}
            detail_arr.append(network_detail)

        return jsonify(detail_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    
    
@app.route('/update_network/<id>' ,methods=['PUT'])
def update_network(id):
    try:
        get_nw_update = request.get_json()
        network_per = get_nw_update['network_per']
    
        sql_update = "UPDATE network SET network_per= %s,update_date= %s WHERE id = %s"
        data_cursor.execute(sql_update,(network_per,date_now,id))
        database_connect.commit()
        return("Update success")
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    
    
@app.route('/delete_network/<id>' ,methods=['DELETE'])
def delete_network(id):
    try:
        sql_update = "DELETE FROM network WHERE id = %s"
        data_cursor.execute(sql_update,(id,))
        database_connect.commit()
        return("Delete success")
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    


# Cloud
@app.route('/add_cloud' ,methods=['POST'])
def add_data_cloud():
    try:
        data_cloud = request.get_json()

        cloud_platform = data_cloud['cloud_platform']
        cloud_per = data_cloud['cloud_per']
        pcidss = data_cloud['pcidss']
        health_care = data_cloud['health_care']
        data_type = data_cloud['type']

        for (platform,percent) in zip(cloud_platform, cloud_per):
            sql_insert = "INSERT INTO cloud ( cloud_platform, cloud_per, pcidss, health_care, type, create_date, update_date ) VALUES (%s,%s,%s,%s,%s,%s,%s)"
            data_cursor.execute(sql_insert,(platform,percent,pcidss,health_care,data_type,date_now,date_now))
            database_connect.commit()
        return ("Insert success")
        
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/cloud_month' ,methods=['GET'])
def get_cloud_month():
    try:
        sql_select = "SELECT FORMAT(Avg(cloud_per+pcidss+health_care)/3,2), month(update_date) FROM cloud WHERE YEAR(update_date) = %s GROUP BY month(update_date)"
        data_cursor.execute(sql_select,(year_now,))
        result = data_cursor.fetchall()

        avg_arr = []
        for avg in result:
            status = avg[0]
            month = calendar.month_name[avg[1]]
            norm_month = avg[1]
            data_set = {"norm_month":norm_month,"month":month,"status":status}
            avg_arr.append(data_set)
           
        return jsonify(avg_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/cloud_detail' ,methods=['POST'])
def get_cloud_detail():
    try:
        get_month = request.get_json()
        month = get_month['month']

        sql_select = "SELECT cloud_platform, FORMAT(Avg(cloud_per),2) FROM cloud WHERE MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY cloud_platform"
        data_cursor.execute(sql_select,(month,year_now))
        result = data_cursor.fetchall()

        detail_arr = []
        for detail in result:
            network = detail[0]
            status = detail[1]
            cloud_detail = {"network":network,"status":status}
            detail_arr.append(cloud_detail)

        return jsonify(detail_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/update_cloud/<id>' ,methods=['PUT'])
def update_cloud(id):
    try:
        get_nw_update = request.get_json()
        cloud_per = get_nw_update['cloud_per']
        pcidss = get_nw_update['pcidss']
        health_care = get_nw_update['health_care']
        data_type = get_nw_update['type']
    
        sql_update = "UPDATE cloud SET cloud_per= %s,pcidss= %s,health_care= %s,type= %s,update_date= %s WHERE id = %s"
        data_cursor.execute(sql_update,(cloud_per,pcidss,health_care,data_type,date_now,id))
        database_connect.commit()
        return("Update success")
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/delete_cloud/<id>' ,methods=['DELETE'])
def delete_cloud(id):
    try:
        sql_update = "DELETE FROM cloud WHERE id = %s"
        data_cursor.execute(sql_update,(id,))
        database_connect.commit()
        return("Delete success")
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    


@app.route('/compliance_month' ,methods=['GET'])
def get_compliance_month():
    try:
        sql_select = """SELECT FORMAT(Avg(pcidss+health_care)/2,2), month(update_date) FROM cloud WHERE type = "Compliance Cloud" AND YEAR(update_date) = %s GROUP BY month(update_date)"""
        data_cursor.execute(sql_select,(year_now,))
        result = data_cursor.fetchall()

        avg_arr = []
        for avg in result:
            status = avg[0]
            month = calendar.month_name[avg[1]]
            norm_month = avg[1]
            data_set = {"norm_month":norm_month,"month":month,"status":status}
            avg_arr.append(data_set)
           
        return jsonify(avg_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/compliance_detail' ,methods=['POST'])
def get_compliance_detail():
    try:
        get_month = request.get_json()
        month = get_month['month']

        sql_select = """SELECT pcidss, health_care, FORMAT(Avg(pcidss),2), FORMAT(Avg(health_care),2) FROM cloud WHERE type = "Compliance Cloud" AND MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY pcidss,health_care"""
        data_cursor.execute(sql_select,(month,year_now))
        result = data_cursor.fetchall()

        detail_arr = []
        for detail in result:
            pcidss = detail[2]
            health_care = detail[3]
            cloud_detail = {"pcidss":pcidss,"health_care":health_care}
            detail_arr.append(cloud_detail)

        return jsonify(detail_arr)
    
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

# Network and Cloud 
@app.route('/add_all_data' ,methods=['POST'])
def add_all_data():
    try:
        data = request.get_json()

        platform_network = data['platform_network']
        network_per = data['network_per']
        cloud_platform = data['cloud_platform']
        cloud_per = data['cloud_per']
        pcidss = data['pcidss']
        health_care = data['health_care']
        data_type = data['type']

        for (platform,percent) in zip(platform_network, network_per):
            sql_insert = "INSERT INTO network ( platform_network, network_per, create_date, update_date ) VALUES (%s,%s,%s,%s)"
            data_cursor.execute(sql_insert,(platform,percent,date_now,date_now))
            database_connect.commit()
        
        for (platform,percent) in zip(cloud_platform, cloud_per):
            sql_insert = "INSERT INTO cloud ( cloud_platform, cloud_per, pcidss, health_care, type, create_date, update_date ) VALUES (%s,%s,%s,%s,%s,%s,%s)"
            data_cursor.execute(sql_insert,(platform,percent,pcidss,health_care,data_type,date_now,date_now))
            database_connect.commit()

        return ("Insert success")

               
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

@app.route('/sla' ,methods=['GET'])
def get_sla():
    try:
        #network
        sql_select = "SELECT platform_network, FORMAT(Avg(network_per),2) FROM network WHERE MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY platform_network"
        data_cursor.execute(sql_select,(month_now,year_now))
        network_result = data_cursor.fetchall()
        len_network_result = len(network_result)
        network_sum = 0
        nw_arr = []
        for sla_network_detail in network_result:
            network = sla_network_detail[0]
            status = sla_network_detail[1]
            network_sum += float(sla_network_detail[1])
            dd = {
                "network":network,
                "status":status
            }
            nw_arr.append(dd)

        network_avg = network_sum/len_network_result

        #cloud computing
        sql_select = "SELECT cloud_platform, FORMAT(Avg(cloud_per+pcidss+health_care)/3,2) FROM cloud WHERE MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY cloud_platform"
        data_cursor.execute(sql_select,(month_now,year_now))
        cloud_result = data_cursor.fetchall()
        len_cloud_result = len(cloud_result)
        cloud_sum = 0
        cloud_arr = []
        for sla_cloud_detail in cloud_result:
            cloud = sla_cloud_detail[0]
            status = sla_cloud_detail[1]
            cloud_sum += float(sla_cloud_detail[1])
            vv = {
                "cloud":cloud,
                "status":status
            }
            cloud_arr.append(vv)
   
        cloud_avg = cloud_sum/len_cloud_result

        #compliance cloud
        sql_select = """SELECT cloud_platform, FORMAT(Avg(pcidss+health_care)/2,2) FROM cloud WHERE type = "Compliance Cloud" AND MONTH(update_date) = %s AND YEAR(update_date) = %s GROUP BY cloud_platform"""
        data_cursor.execute(sql_select,(month_now,year_now))
        compliance_result = data_cursor.fetchall()
        compliance_arr = []
        for sla_compliance_detail in compliance_result:
            compliance = sla_compliance_detail[0]
            status = sla_compliance_detail[1]
            vv = {
                "compliance":compliance,
                "status":status
            }
            compliance_arr.append(vv)

        sla_format = {
            "network_avg":"{:.2f}".format(network_avg),
            "current_network":nw_arr,
            "cloud_avg":"{:.2f}".format(cloud_avg),
            "current_cloud":cloud_arr,
            "current_compliance":compliance_arr
        }
        
        return jsonify(sla_format)
        
 
    except Exception as e:
        print("error >>>>>>",e)
        return ("error >>>>>>",e)
    

          
if __name__ == "__main__" :
    app.run(debug=True)
